# Tsukudani with Kombu Seaweed and Shiitake Mushrooms
I prepare this recipe with the leftover kombu seaweed and shiitake mushrooms that I use to make my vegan dashi for the miso soup. I found the original recipe some years ago on [Cookpad](https://cookpad.com/uk/recipes/152664-tsukudani-from-leftover-shiitake-mushroom-and-kombu-after-making-dashi-stock-macrobiotic).

## Time
15 minutes

## Ingredients
* 2 slices of kombu seaweed 15x5cm
* 4 Shiitake mushrooms
* 3 tbsp dashi broth
* 1/2 tbsp Vinegar
* 1 tbsp Soy sauce
* 1 tsp roasted sesame or 1/2 tsp gomasio

## Servings
3

## Equipment
* pan
* knife

## Procedure
1. Slice kombu seaweeds into 3-4mm strips. Remove the mushroom stalks and slice them;
2. Put seaweeds, mushrooms, dashi broth, soy sauce and vinegar in a pan. Cover and simmer for 5 minutes.
3. Uncover the pan and let the remaining liquid dry out for about a minute, then add a toasted sesame or gomasio, and serve.

## Tag
* vegan
* vegetarian
* dairy free
* fat free
* egg free
* milk protein free