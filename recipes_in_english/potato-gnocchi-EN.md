# Potato gnocchi
Easy to make and delicious, the only thing that can spoil them is the wrong type of potato. Potato gnocchi have only 3 ingredients (it is even possible to do them with 2!) and can be served with pesto, bolognese, tomato sauce, sausage, or just butter and sage...
## Time
45 minutes
- 15 minutes - potatoes cooking time
- 20 minutes - preparation time
- 5 minutes - cooking time
## Ingredients
- 500g medium-sized **floury** potatoes
- 150g all purpose flour
- 1 egg
- 3g salt
## Servings
2
## Equipment
- Pressure cooker (optional)
- Potato masher
- Large salad bowl
- Gnocchi rimming tool (optional), or a fork
- Skimmer
## Procedure
1. Wash the potatoes, and place them with their skins on the basket in the pressure cooker. Add two glasses of cold water. Close and bring to pressure. From the moment the pressure cooker comes under pressure, cook for 15 minutes. If no pressure cooker is available, the potatoes can be cooked in water whole and with the skin on, for about 45 minutes.
2. Vent the pot, take out the potatoes, remove the skins while still hot and mash them immediately in a large salad bowl. Depending on the type of potato masher used, it may not be necessary to peel them first. Cool the mashed potatoes by turning them with a spoon.
3. Add 150g all purpose flour, the salt, and mix gently with a spoon.
4. Add the egg, mix first with the spoon, then work the dough by hand, briefly, just enough to form a ball.
5. Take a piece of dough and, on a lightly floured pastry board, form a snake about 1.5cm in diameter. Cut into pieces of about 2cm, then line the gnocchi with the special tool or with a fork, and place them on a lightly floured tray. Repeat until all the dough is finished. 
6. Bring a pot of generously salted water to the boil (the dough is lightly salted), turn the water until it forms a vortex and pour in all the gnocchi.
7. The gnocchi will be cooked as soon as they rise to the surface. Scoop them up with a skimmer and put them directly into the sauce. Stir gently and serve immediately.
## Tag
* vegetarian
* dairy free
* dairy protein free