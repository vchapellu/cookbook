# Cookbook 
[~ Italian version below ~](#cookbook-italiano)

Cookbook is exactly what its name suggests, a recipe book.
All recipes are written in [Git Lab Flavored Markdown](https://docs.gitlab.com/ee/user/markdown.html), and can be written in both English or Italian. Anyone can contribute and add new recipes following the guidelines.<br>
[Here](/recipe.code-snippets) you can find the VSCode snippet with the recipes template.

## Add a new recipe
### Naming
*recipe-name-EN.md* - for a recipe in English
*nome-della-ricetta-IT.md* - for a recipe in Italian

### Template
Every recipe must contain **at least** the following information in this order:
* **Recipe name** - followed by an optional description
* **Time** - indicate first the total time needed and then preparation time, cooking time, and any waiting time before consumption <br>
e.g. <br>
40 minutes
  + 10 minutes preparation
  + 30 minutes cooking
* **Ingredients** - measurements units in abbreviated form, without spacing <br>
e.g. <br>
  * 100g ground beef
  * 2tbsp olive oil
* **Servings** - indicate only the number
* **Equipment**
* **Procedure** - numbered list. Can be divided into several blocks, with its own numbering (e.g. in case of dishes that require first the preparation of a dough, sauce etc.)

### Tag
At the bottom of each recipe you can add specific tags. For instance:
* dairy free
* milk protein free
* egg free
* gluten free
* vegetarian
* vegan
* no-cook
* fat free
<br>
<br>

# Cookbook - Italiano

Cookbook è il mio libro delle ricette.
Tutte le ricette sono scritte in [Git Lab Flavored Markdown](https://docs.gitlab.com/ee/user/markdown.html) e possono essere scritte in italiano o in inglese. Chiunque può contribuire e inserire nuove ricette seguendo le linee guida.<br>
[Qui](/ricette.code-snippets) il link allo snippet per VSCode con il template della ricetta.

## Inserire una nuova ricetta
### Naming
*recipe-name-EN.md* - per una ricetta in inglese
*nome-della-ricetta-IT.md* - per una ricetta in italiano

### Template
Tutte le ricette devono contenere **almeno** le seguenti informazioni in quest'ordine:
* **Nome della ricetta** - seguito da descrizione opzionale
* **Tempo** - indicare tempo di preparazione, tempo di cottura e eventuale attesa prima della consumazione <br>
Esempio: <br>
40 minuti
  + 10 minuti di preparazione
  + 30 minuti di cottura
* **Ingredienti** - unità di misura in forma abbreviata e senza spaziatura <br>
Esempio: <br>
  * 100g carne di manzo macinata
  * 2tbsp olio evo
* **Porzioni**
* **Utensili**
* **Procedimento** - elenco numerato. Divisibile in diversi blocchi, ognuno con numerazione autonoma (nel caso ad esempio di piatti che richiedono prima la preparazione di un impasto, di una salsa ecc.)

### Tag
In fondo ad ogni ricetta è possibile aggiungere tag specifiche. Ad esempio:
* senza lattosio
* senza proteine del latte
* senza uova
* senza glutine
* vegetariana
* vegana
* senza cottura
* senza burro
* senza grassi


