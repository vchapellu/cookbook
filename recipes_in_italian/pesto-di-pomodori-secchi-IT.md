# Pesto di pomodori secchi
## Tempo
10 minuti

## Ingredienti
* 10 pomodori secchi
* 15 mandorle pelate
* 1 peperoncino
* 10 foglie di basilico fresco
* mezzo spicchio di aglio
* Olio EVO

## Porzioni
3 

## Utensili
* mixer
* padella

## Procedimento
1. Tagliare i pomodori secchi a listarelle di 4-5mm e coprirli con 100ml di acqua bollente;
2. Scaldare le mandorle in padella per padella, rigirarle spesso fino a quando non saranno ben tostate su ciascun lato;
3. Mettere nel mixer i pomodori con la loro acqua di ammollo, le mandorle tostate, un peperoncino senza semi, mezzo spicchio d'aglio e un cucchiaio di olio EVO. Azionare il mixer per una decina di secondi. Aggiungere le foglie di basilico, due cucchiai di olio e un paio di cucchiai di acqua se il composto non è cremoso. Azionare il mixer per una decina di secondi.
4. Il pesto è pronto per essere unito alla pasta.
 
## Tag
* vegan
* vegetariana
* senza lattosio
* senza glutine
* crudista
* senza uova
* senza proteine del latte