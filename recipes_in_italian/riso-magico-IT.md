# Riso Magico One Pot

## Tempo
30 minuti
+ 10 minuti di preparazione
+ 20 minuti di cottura

## Ingredienti
* riso basmati
* 150 g carne di bovino trita
* 5 pomodori secchi
* 15 olive taggiasche sott'olio
* 1 cipolla
* 200 g spinati surgelati
* sale e pepe q.b.
* peperoncino a piacere

## Porzioni 
4

## Utensili
* una pentola con coperchio
* cucchiaio di legno
* tagliere
* coltello
* timer

## Procedimento
1. Cuocere la carne trita a fuoco vivo fino a quando non sarà più rosa;
2. Tagliare la cipolla a rondelle sottili, i pomodori secchi in piccoli pezzi, sminuzzare le olive;
3. Unire un filo d'olio alla carne e aggiungere la cipolla, lasciare andare a fuoco medio per qualche minuto, quindi aggiungere i pomodori, gli spinaci e le olive. Cuocere insieme gli ingredienti per 3-4 minuti continuando a mescolare con il cucchiaio;
4. Aggiungere una ciotola e mezza di riso basmati, due ciotole di acqua a temperatura ambiente, sale e pepe. Mischiare e chiudere con il coperchio;
5. Appena inizia a bollire, abbassare la fiamma e fissare un timer a 12 minuti. **Non alzare mai il coperchio**, se rischia di straboccare abbassare la fiamma;
6. Scaduti i 12 minuti, **senza togliere il coperchio**, spostare la pentola dal fuoco e lasciare riposare per 7 minuti.
7. Scoperchiare e servire direttamente in tavola. Aggiungere peperoncino a piacere.

## Tag
* senza lattosio
* senza proteine del latte
* senza uova
* senza glutine