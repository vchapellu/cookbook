# Riso Cantonese
Questa è una ricetta vegetariana ma volendo si può aggiungere prosciutto/pancetta a dadini. Si può fare sia con del riso parboiled che con riso basmati/jasmine cotto al vapore.

## Tempo
30 minuti
+ 10 minuti di preparazione
+ 20 minuti di cottura

## Ingredienti
* 1,5 tazze di riso basmati o jasmine
* 2 tazze di acqua
* 1 carota
* 1 cipollotto
* 3 uova
* 1 tazza di piselli freschi o surgelati
* 1 cucchiaino di mirin
* Pepe

## Porzioni
4

## Utensili
* Pentola con coperchio
* Padella
* Insalatiera/grande piatto da portata
* Coltello
* Tagliere
* Timer
* Pelapatate (opzionale)
* Accessorio julienne (opzionale)

## Procedimento
Per il riso
1. Versare nella pentola una tazza e mezza di riso basmati o jasmine, due tazze di acqua e mezzo cucchiaino di sale. 
2. Coprire la pentola con il coperchio e accendere il fuoco. Appena inizia a bollire, portare la fiamma a medio/basso e impostare il timer su 12 minuti. **Non alzare MAI il coperchio**.
3. Trascorsi i 12 minuti, spostare il riso e lasciare riposare per 7 minuti, sempre senza mai alzare il coperchio.

Per il condimento
1. Sbucciare la carota e tagliare a julienne e tagliare la parte bianca del cipollotto (tenere da parte il verde). Saltare tutto in padella con un filo d'olio per 5 minuti. Finita la cottura mettere tutto nell'insalatiera.
2. Bollire i piselli per una decina di minuti con un pizzico di sale. A fine cottura scolare e aggiungere al resto nell'insalatiera.
3. In un piatto fondo o in una ciotola sbattere le tre uova con un cucchiaino di mirin, pepe a piacere e la parte verde del cipollotto tagliata a fettine sottili. Saltare brevemente le uova in padella con un filo d'olio. Togliere dal fuoco appena si addensano e aggiungere nell'insalatiera insieme agli altri ingredienti del condimento.
4. Unire anche il riso e 3 cucchiai di salsa di soia nell'insalatiera. Mescolare bene e servire.

## Tag
* senza lattosio
* senza proteine del latte
* vegetariana
* senza glutine