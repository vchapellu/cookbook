# Pesto di broccoli e mandorle

## Tempo
20 minuti
## Ingredienti
- 150g cime di broccoli pulite
- 8-10 mandorle
- parmigiano 
- 1 peperoncino
- 1 spicchio d'aglio
- olio evo
- sale
## Porzioni
2
## Utensili
- padella
- mixer
## Procedimento
1. tritare grossolanamente le mandorle nel mixer e mettere da parte in una ciotola;
1. lessare i broccoli per 6-7 minuti e poi frullarli nel mixer con 4-5 cucchiai di acqua di cottura;
1. tostare le mandorle in padella, quando saranno leggermente imbrunite aggiungere l'aglio, il peperoncino tritato e due cucchiai di olio. Lasciare tostare per un paio di minuti;
1. unire in padella le mandorle con la crema di broccoli, aggiungere il parmigiano e se ne necessario qualche cucchiaio di acqua di cottura dei broccoli;
1. unire il pesto alla pasta e buon appetito!
## Tag
- vegetariana