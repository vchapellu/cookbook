# Polpette di carne
Una ricetta per le classiche polpette di carne da cuocere in umido con il sugo e mangiare con le linguine. Usando la stessa base è possibile fare le polpette fritte.

## Tempo
40 minuti
+ 10 minuti di preparazione
+ 30 minuti di cottura

## Ingredienti
* 150 g carne di bovino o di maiale trita
* 1 uovo
* 2 cucchiai di pan grattato
* 200 g di pelati
* 1 cucchiaio di salsa di soia
* aglio in polvere
* sale
* pepe
* prezzemolo
* origano
* olio evo
* acqua

## Porzioni
4

## Utensili
* una pentola con coperchio
* cucchiaio di legno
* pinza per friggere (opzionale)
* coltello o forbici
* timer

## Procedimento
1. Mettere la carne in un piatto o in una scodella, aggiungere l'uovo intero e mischiare bene fino a quando uovo e carne saranno ben amalgamati.
2. Aggiungere il pan grattato e aglio in polvere, prezzemolo tritato, sale e pepe a piacere quindi amalgamare bene tutti gli ingredienti. Il composto dovrà essere abbastanza solido da poter formare delle polpette, se è troppo morbido occorrerà aggiungere un po' di pan grattato fino a raggiungere la consistenza desiderata.
3. Formare con le mani delle polpette di circa 3cm di diametro.
4. Scaldare due cucchiai di olio in una padella, quando sarà caldo aggiungere le polpette e rosolarle bene a fuoco vivo su ogni lato aiutandosi con una pinza o con una forchetta.
5. Quando le polpette saranno ben rosolate abbassare il fuoco e aggiungere i pelati spezzettati, l'origano, un cucchiaio di salsa di soia e circa 100 grammi di acqua. Mescolare bene. 
6. Quando il composto inizierà a bollire, abbassare il fuoco al minimo. Coprire con il coperchio e cuocere per 30 minuti. Ogni 5-10 minuti alzare il coperchio e girare le polpette.
7. A fine cottura aggiungere un po' di pepe e aggiustare il sale. Le polpette sono pronte per essere saltate con la pasta o impiattate con un contorno di riso, purée di patate, verdure... 

## Tag
* senza lattosio
* senza proteine del latte