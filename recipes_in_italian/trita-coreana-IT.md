# Trita Coreana
Questa è la ricetta base per marinare la carne trita. Si abbina bene in un bowl con carote julienne e broccoli saltati in padella e sfumati con salsa di soia e riso basmati/jasmine

## Tempo
20 minuti
+ 10 minuti di preparazione
+ 10 minuti di cottura

## Ingredienti
* 250 g carne di bovino trita
* 2 spicchi di aglio
* 1/2 cipolla
* 2tbsp salsa di soia
* 1 tbsp mirin
* 1 tbsp zucchero di canna
* 2 cipollotti
* 1 peperoncino
* Semi di sesamo
* Olio di sesamo

## Porzioni
+ 2 se accompagnato solo da riso
+ 4 se accompagnato da riso e verdure

## Utensili
* misurino graduato per tbsp
* padella con coperchio
* coltello
* tagliere
* cucchiaio di legno
* schiaccia-aglio (opzionale)

## Procedimento
1. Tritare finemente mezza cipolla e il peperoncino (eliminare i semi), tagliare a fettine sottili la parte bianca dei cipollotti (tenere il verde da parte), schiacciare con lo schiaccia-aglio o tritare finemente i due spicchi di aglio.
2. In una scodella mettere tutti gli ingredienti del punto 1 e aggiungere la carne, la salsa di soia, il mirin e lo zucchero. Amalgamare bene tutti gli ingredienti e lasciare riposare per qualche minuto.
3. Scaldare una padella a fuoco alto, quando sarà ben calda mettere la carne con la sua marinatura e far cuocere facendo a piccoli pezzi la carne con il cucchiaio di legno. Quando la carne non è più rosa, abbassare a fuoco medio/basso, mettere il coperchio e cuocere per 5 minuti.
4. Tagliare la parte verde dei cipollotti a fettine sottili.
5. Togliere dal fuoco carne e aggiungere semi di sesamo a piacere, due cucchiai di olio di sesamo e i cipollotti tritati. Mescolare bene e servire.

## Tag
* senza lattosio
* senza proteine del latte
* senza uova
* senza glutine