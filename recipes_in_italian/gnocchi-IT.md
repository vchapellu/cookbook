# Gnocchi di patate
Facili da fare e buonissimi, l'unica cosa che può rovinarli è il tipo sbagliato di patata. Gli gnocchi di patate hanno solo 3 ingredienti e possono essere conditi con pesto, ragù, sugo di pomodoro, salsiccia, burro e salvia...

## Tempo
45 minuti
+ 15 minuti di cottura delle patate
+ 20 minuti di preparazione
+ 5 minuti di cottura

## Ingredienti
- 500g di patate **farinose** di grandezza media
- 150g di farina00
- 1 uovo

## Porzioni
2

## Utensili
* Pentola a pressione (opzionale)
* Schiaccia-patate
* Grande insalatiera
* Attrezzo per rigare gli gnocchi (opzionale)
* Spumarola

## Procedimento
1. Lavare le patate e posizionare le patate con la buccia sul cestello nella pentola a pressione. Aggiungere due bicchieri di acqua fredda. Chiudere e portare in pressione. Dal momento in cui la pentola va in pressione cuocere per 15 minuti. Se non è disponibile una pentola a pressione, le patate possono essere cotte in acqua intere e con la buccia, per circa 45minuti.
2. Sfiatare la pentola, estrarre le patate, sbucciarle ancora calde e schiacciarle immediatamente in una grande insalatiera. A seconda del tipo di schiaccia-patate utilizzato, potrebbe non essere necessario sbucciarle prima. Raffreddare le patate schiacciate rigirandole con un cucchiaio.
3. Aggiungere 150g di farina00, mescolare bene con il cucchiaio.
4. Aggiungere un uovo, mescolare con il cucchiaio e poi impastare a mano brevemente, quel che basta per formare una palla.
5. Prendere un pezzo di impasto e, sulla spianatoia leggermente infarinata, formare un serpente di circa 1,5cm di diametro. Tagliare a pezzetti di circa 2,5cm fino a quando tutto l'impasto non sarà finito. Quindi rigare gli gnocchi con l'apposito attrezzo oppure con una forchetta e posizionarli su un vassoio leggermente infarinato.
6. Far bollire una pentola d'acqua **abbondantemente salata** (l'impasto non è salato!) girare l'acqua fino a formare un vortice e versare tutti gli gnocchi.
7. Gli gnocchi saranno cotti non appena verranno a galla. Raccoglierli con una spumarola e metterli direttamente nel sugo di condimento. Mescolare delicatamente e servire subito.

## Tag
* vegetariana
* senza latte
* senza lattosio
* senza proteine del latte
