# Quiche di porri
La classica quiche con porri e patate, ma vegetariana. Si accompagna bene con un'insalata.

## Tempo
45 minuti
+ 15 minuti di prepaparazione
+ 30 minuti di cottura

## Ingredienti
* 1 rotolo di [pasta brisée](https://www.cucchiaio.it/ricetta/ricetta-pasta-brisee/)
* 1 porro grande (diametro 2,5cm ca.) oppure 2 piccoli
* 2 patate medie
* Tofu affumicato
* 3 uova
* 3 cucchiai di crème fraîche (o alternativa vegana)
* 2 cucchiai di senape
* Aneto
* Olio
* Sale
* Pepe

## Porzioni
4

## Utensili
* teglia da forno tonda con bordo alto 2-3 cm
* padella con coperchio
* scodella
* coltello
* forchetta
* tagliere

## Procedimento
1. Lavare bene le due patate con la buccia e poi tagliarle a fettine di circa 5 mm.
2. Scaldare un cucchiaio di olio nella padella e fare saltare a fuoco vivo le patate per un paio di minuti avendo cura di girarle da entrambi i lati. Abbassare il fuoco, coprire le patate e lasciare cuocere per altri 10 minuti.
3. Accendere il forno a 200° 
4. Tagliare il porro a fettine di 2-3 mm quindi saltarlo in padella con due cucchiai di olio per un paio di minuti. Aggiungere due cucchiai di acqua, una spolverata di sale e cuocere a fuoco basso con il coperchio per 5 minuti.
5. Lasciare raffreddare le patate in un piatto mentre si stende la pasta brisée sulla teglia tonda. Il riempimento sarà liquido quindi la pasta deve coprire anche i bordi della teglia.
6. Posizionare le patate sul fondo della teglia cercando di metterle vicine ma non sovrapposte. Se ne avanzano si potranno aggiungere successivamente sopra ai porri. 
7. Tagliare il tofu a dadini e distribuirlo sopra le patate.
8. Ora che i porri sono cotti distribuirli uniformemente sopra alle patate e al tofu. Aggiungere sopra le eventuali patate avanzate e una spolverata di sale.
9. Rompere le tre uova nella scodella, sbatterle con una frusta o forchetta, aggiungere 3 cucchiai di crème fraîche, 2 cucchiai di senape, 1 cucchiaino di aneto, sale e pepe. Amalgamare bene tutti gli ingredienti con la frusta/forchetta.
10. Versare la crema nella teglia sopra le verdure e inclinare la teglia in tutte le direzioni così che la crema si distribuisca bene e non rimangano buchi. 
11. Ripiegare verso l'interno della teglia la pasta in eccesso sui bordi (o rimuoverla se si preferisce aperta) e infornare a 200° per 15-20 minuti, fino a quando la superficie non sarà dorata.

## Tag
* vegetariana