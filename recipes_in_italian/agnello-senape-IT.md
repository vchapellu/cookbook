# Bistecche di agnello marinate alla senape

## Tempo
9 ore
+ 10 minuti di preparazione
+ 8 ore di attesa
+ 50 minuti di cottura

## Ingredienti
* 8 bistecche di agnello
* 60g olio di oliva
* 60g succo di limone
* 1 spicchi d'aglio
* 15g salsa di soia
* 30g senape
* sale
* pepe

## Porzioni
4

## Utensili
* Contenitore ermetico per la marinatura (per esempio, una pirofila con coperchio)
* Griglia
* Pirofila

## Procedimento
Preparare la marinatura
1. Sminuzzare l'aglio
1. Adagiare le bistecche nel contenitore ermetico assieme a tutti gli altri ingredienti
1. Lasciare riposare almeno otto ore, agitando e ruotando il contenitore di tanto in tanto

Cuocere
1. Scaldare la griglia o la padella
1. Scottare le bistecche un minuto per lato
1. Adagiare le bistecche nella pirofila, ricoprirle con la marinatura
1. Cuocere in forno per circa 30 minuti
1. Sfornare e lasciare riposare per qualche ulteriore minuto

## Tag
* carne
* lunga attesa
