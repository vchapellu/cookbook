# Plum Cake salato
Ricetta di base del plum cake salato, si possono aggiungere olive, zucchine, cipolle, pomodorini, prosciutto...
Si può mangiare sia freddo che caldo.
## Tempo
60 minuti
- 20 minuti di preparazione
- 40 minuti di cottura
## Ingredienti
- 250g di farina 00
- 3 uova
- 200g di latte di avena
- 60 grammi di olio evo
- una bustina di lievito di birra secco
- sale (5-7g a seconda del ripieno)
- pepe
- origano
## Porzioni
4
## Utensili
- Sbattitore o planetaria
- teglia da plum cake stretta e lunga oppure pirottini per muffin
- forno
## Procedimento
1. Preparare tutti gli ingredienti per il ripieno, cuocerli se necessario e tenerli da parte;
2. Con la planetaria o con lo sbattitore sbattere le uova, unire l'olio a filo e quindi aggiungere il latte;
3. Accendere il forno a 170° ventilato.
4. Unire in una scodella la farina, il lievito, il sale, il pepe e l'origano. Mescolare bene e poi unire al resto dell'impasto di cui al punto 2. Mescolare bene con lo sbattitore o con la planetaria fino ad avere un impasto liscio e morbido.
4. Unire delicatamente il ripieno all'impasto, mescolando con un cucchiaio dal basso verso l'alto. 
5. Versare l'impasto in una teglia da plum cake imburrata e infarinata o nei pirottini per muffin. 
6. Infornare a 170° per 35/40minuti
## Tag
- senza latte
- vegetariana