# Strudel di mele - Draft
## Ingredienti
- Per la pasta
  - 140g di farina 0
  - 30g di latte (o latte vegetale o acqua)
  - 1 uovo
  - 1 pizzico di sale
- Per il ripieno
  - 700g di mele
  - 20g di pinoli
  - 1cucchiaino di cannella
  - 1 limone
  - 60g di pan grattato
  - 5 amaretti