# Zuppa di Legumi 
Un ricco piatto unico dal sapore invernale.

## Tempo
40 minuti
+ 10 minuti di preparazione
+ 30 minuti di cottura

## Ingredienti
* 1 carota
* 1 cipolla
* 1 costa di sedano
* 1 spicchio d'aglio
* 1 rametto di rosmarino
* 5 grammi di funghi porcini secchi
* 3 pomodorini secchi
* 50 g lenticchie disidratate
* 50 g farro disidratato
* 50 g orzo disidratato
* 100 g riso integrale
* 100 g riso rosso
* Pancetta a piacere (opzionale)
* Sale o brodo granulare q.b.
* Olio di oliva q.b.

## Porzioni
4

## Utensili
* Tagliere
* Coltello
* Pentola con coperchio
* Timer
* Mixer (opzionale)

## Procedimento
1. Pulire carota, cipolla, sedano e aglio. Tagliare i pomodorini a pezzetti.
2. Mettere nel mixer (o tritare al coltello) tutti gli ingredienti del punto 1, aggiungere le foglie di rosmarino e i funghi e tritare tutto grossolanamente.
3. Mettere a bollire un litro e mezzo di acqua.
4. Mettere due cucchiai di olio EVO nella pentola, accendere il fuoco medio-alto e aggiungere la pancetta (opzionale) e tutti gli ingredienti tritati con il mixer.
5. Lavare le lenticchie, il farro e l'orzo, quindi aggiungerli nella pentola. Aggiungere anche i risi. 
6. Salare o aggiungere il brodo granulare. Mischiare bene e coprire con l'acqua bollita.
7. Coprire e cuocere per 30 minuti.
8. Aggiungere pepe e aromatiche a piacere e servire.

## Tag
* senza uova
* vegetariano
* vegano
* senza lattosio
* senza proteine del latte