# Tsukudani alga kombu e funghi shiitake 
Ogni tanto preparo il brodo dashi vegano per la zuppa di miso e con l'alga kombu e i funghi shiitake avanzati preparo questa insalatina leggera e saporita che ho trovato su [Cookpad](https://cookpad.com/uk/recipes/152664-tsukudani-from-leftover-shiitake-mushroom-and-kombu-after-making-dashi-stock-macrobiotic) qualche anno fa.

## Tempo
15 minuti

## Ingredienti
* 2 strisce di alga kombu 15x5cm
* 4 funghi shiitake
* 6 cucchiai di brodo dashi
* 1 cucchiaio di aceto
* 2 cucchiai di salsa di soia
* 1 cucchiaino di sesamo tostato o 1/2 cucchiaino di gomasio

## Porzioni
3

## Utensili
* padella
* coltello

## Procedimento
1. Affettare l'alga a strisce di 3-4mm. Togliere i gambi dei funghi e affettarli;
2. Mettere in padella le alghe, i funghi, 6 cucchiai di dashi, 2 cucchiai di salsa di soia e 1 cucchiaio di aceto. Coprire e lasciare cuocere a fuoco medio-basso per 5 minuti.
3. Scoperchiare la padella e lasciare asciugare il liquido rimanente per circa un minuto, quindi impiattare e aggiungere un cucchiaino di sesamo tostato o mezzo cucchiaino di gomasio.

## Tag
* vegana
* vegetariana
* senza latte
* senza grassi
* senza uova
* senza proteine del latte